import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget RoundButton({
  required Color color,
  Widget? child,
  VoidCallback? onTap,
}) {
  return InkWell(
    customBorder: CircleBorder(),
    child: Container(
      padding: EdgeInsets.all(8.0),
      decoration: BoxDecoration(
          color: color,
          border: Border.all(
            color: color,
          ),
          borderRadius: BorderRadius.all(Radius.circular(50))),
      child: child,
    ),
    onTap: onTap,
  );
}
