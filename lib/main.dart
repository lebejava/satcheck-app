import 'dart:async';
import 'dart:io';
import 'dart:isolate';
import 'dart:typed_data';

import 'package:archive/archive.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:path_provider/path_provider.dart';
import 'package:satcheck/map.dart';
import 'package:satcheck/round_button.dart';
import 'package:satcheck/singleton.dart';
import 'dart:math' as math;
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'package:usb_serial/transaction.dart';
import 'package:usb_serial/usb_serial.dart';

void main() {
  runApp(const MyApp());
}

int prevSecond = 0;
Singleton singleton = new Singleton();

mainProcess(SendPort sendPort) async {
  // TODO: Main logic (tacker, boxes and other locations)
  print('start');

  print('end');

  int sum = 0;
  sendPort.send(sum);
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
      builder: EasyLoading.init(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Singleton singleton = new Singleton();

  bool loading = false;
  String? _dir;
  List<String>? _images = [];
  String _timeString = '00:00:00';

  String _zipPath = 'https://mota-engil.murcielago.dev/map.zip';
  String _localZipFileName = 'images.zip';

  // USB
  String _status = "Idle";
  List<Widget> _ports = [];
  List<String> _serialData = [];

  StreamSubscription<String>? _subscription;
  Transaction<String>? _transaction;
  UsbDevice? _device;

  bool isConnecting = false;

  Timer? timer;
  int prevSecond = 0;
  bool isProcessing = false;

  late final receivePort = ReceivePort();

  @override
  void initState() {
    super.initState();
    SystemChrome.setEnabledSystemUIOverlays([]);
    _initDir();
    updateTime();
    timer = Timer.periodic(Duration(seconds: 1), (Timer t) => updateTime());

    UsbSerial.usbEventStream!.listen((UsbEvent event) {
      _getPorts();
    });

    _getPorts();
  }

  @override
  void dispose() {
    super.dispose();
    _connectTo(null);
    timer!.cancel();
  }

  void updateTime() async {
    final DateTime now = DateTime.now();

    singleton.unix = now.millisecond;

    if (prevSecond != now.second) {
      // Display Time
      final String formattedDateTime = DateFormat('HH:mm:ss').format(now);
      setState(() {
        _timeString = formattedDateTime;
        prevSecond = now.second;
        print(prevSecond);
      });

      // Main process
      if (!isProcessing && prevSecond % 10 == 0) {
        setState(() {
          isProcessing = true;
        });

        // Isolate
        if (singleton.port != null) {
          if (!singleton.isReadyToSend) {
            rutine();
          } else {
            print(
                'Ready to Send ${singleton.myCurrentLocation.latitude}, ${singleton.myCurrentLocation.longitude}');

            sendLocationToServer();
          }
        } else {
          print("USB Null");
        }

        setState(() {
          isProcessing = false;
        });

        // await Isolate.spawn(mainProcess, receivePort.sendPort);
      }
    }
  }

  _initDir() async {
    setState(() {
      isProcessing = false;
    });
    if (null == _dir) {
      _dir = (await getApplicationDocumentsDirectory()).path;
      print("init $_dir");
    }

    singleton.localDir = _dir!;

    receivePort.listen((message) {
      print(message);
      setState(() {
        isProcessing = false;
      });
    });
  }

  Future<void> update() async {
    setState(() {
      loading = true;
    });

    EasyLoading.show(
      status: 'Actualizando...',
    );
    _images?.clear();

    var zippedFile = await _downloadFile(_zipPath, _localZipFileName);
    await unarchiveAndSave(zippedFile);

    singleton.localDir = (await getApplicationDocumentsDirectory()).path;

    EasyLoading.dismiss();

    setState(() {
      loading = false;
    });
  }

  Future<File> _downloadFile(String url, String fileName) async {
    var req = await http.Client().get(Uri.parse(url));
    var file = File('$_dir/$fileName');
    print("file.path ${file.path}");
    return file.writeAsBytes(req.bodyBytes);
  }

  unarchiveAndSave(var zippedFile) async {
    var bytes = zippedFile.readAsBytesSync();
    var archive = ZipDecoder().decodeBytes(bytes);
    for (var file in archive) {
      var fileName = '$_dir/${file.name}';
      print("fileName ${fileName}");
      if (file.isFile && !fileName.contains("__MACOSX")) {
        var outFile = File(fileName);
        //print('File:: ' + outFile.path);
        _images?.add(outFile.path);
        outFile = await outFile.create(recursive: true);
        await outFile.writeAsBytes(file.content);
      }
    }
  }

  Future<bool> _connectTo(device) async {
    _serialData.clear();

    if (_subscription != null) {
      _subscription!.cancel();
      _subscription = null;
    }

    if (_transaction != null) {
      _transaction!.dispose();
      _transaction = null;
    }

    if (singleton.port != null) {
      singleton.port!.close();
      singleton.port = null;
    }

    if (device == null) {
      _device = null;
      // Disconnected
      return true;
    }

    singleton.port = await device.create("", 3);
    if (await (singleton.port!.open()) != true) {
      // Failed to open port
      return false;
    }
    _device = device;

    await singleton.port!.setDTR(false);
    await singleton.port!.setRTS(false);
    await singleton.port!.setPortParameters(
        115200, UsbPort.DATABITS_8, UsbPort.STOPBITS_1, UsbPort.PARITY_NONE);

    _transaction = Transaction.stringTerminated(
        singleton.port!.inputStream as Stream<Uint8List>,
        Uint8List.fromList([13, 10]));

    _subscription = _transaction!.stream.listen((String line) {
      switch (singleton.commandIndex) {
        case 0:
          print('caso 0');
          if (line.indexOf('OK') != -1) {
            print('AT OK');
            singleton.commandIndex = 1;
          }
          break;
        case 1:
          print('caso 1');
          if (line.indexOf('+CPIN: READY') != -1) {
            print('READY');
            singleton.commandIndex = 2;
          }
          break;
        case 2:
          print('caso 2');
          if (line.indexOf('+CREG: 0,1') != -1 ||
              line.indexOf('+CREG: 0,5') != -1) {
            print('REGISTER');
            singleton.commandIndex = 3;
            singleton.isReadyToSend = true;
          }
          break;
        case 3:
          print('caso 3');
          print(line);
          break;
        case 4:
          print('caso 4');
          break;
      }
    });

    return true;
  }

  void _getPorts() async {
    _ports = [];
    List<UsbDevice> devices = await UsbSerial.listDevices();
    if (!devices.contains(_device)) {
      _connectTo(null);
    }

    devices.forEach((device) {
      print(device);
      /*print(
          '${device.productName}|${device.manufacturerName} -> ${isConnecting}');*/

      if (device.productName == 'A76XX Series LTE Module' && !isConnecting) {
        _connectTo(device);
        singleton.isConnecting = true;
      }
    });
  }

  void rutine() async {
    if (singleton.port == null) {
      print('No USB Port');
      return;
    }

    String data = "${singleton.commands[singleton.commandIndex]}\r\n";
    await singleton.port!.write(Uint8List.fromList(data.codeUnits));
  }

  void sendLocationToServer() async {
    if (singleton.port == null) {
      print('No USB Port');
      return;
    }

    var _list = Uint8List.fromList([
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      13,
      10
    ]);

    var byteData = _list.buffer.asByteData();

    byteData.setInt8(0, 'T'.codeUnitAt(0));
    byteData.setInt8(1, 40);
    byteData.setInt16(2, 800, Endian.little);
    byteData.setInt32(4, 0x80102030, Endian.little);
    byteData.setInt32(8, (singleton.unix - 946702800), Endian.little);
    byteData.setFloat32(
        12, singleton.myCurrentLocation.latitude!, Endian.little);
    byteData.setFloat32(
        16, singleton.myCurrentLocation.longitude!, Endian.little);
    byteData.setInt8(20, singleton.myCurrentLocation.speed!.toInt());
    byteData.setInt16(
        21, singleton.myCurrentLocation.bearing!.toInt(), Endian.little);

    String data =
        'AT+CIPSEND=1,${byteData.buffer.lengthInBytes},"controlporsatelite.com",32010\r\n';
    await singleton.port!.write(Uint8List.fromList(data.codeUnits));

    await singleton.port!.write(byteData.buffer.asUint8List());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          SafeArea(
            child: SingleChildScrollView(
              child: Row(
                children: [
                  Container(
                    width: 100.0,
                    height: MediaQuery.of(context).size.height,
                    color: Colors.black,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Padding(
                          padding: EdgeInsets.all(8.0),
                          child: RoundButton(
                            color: Color(0xFFF6B5B5),
                            child: Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Image.asset('assets/mail.png'),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(8.0),
                          child: RoundButton(
                            color: Color(0xFFB8EBA3),
                            child: Padding(
                              padding: EdgeInsets.all(8),
                              child: Image.asset('assets/papel.png'),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(8.0),
                          child: RoundButton(
                            color: Color(0xFF92BED8),
                            child: Padding(
                              padding: EdgeInsets.all(8),
                              child: Image.asset('assets/megaphone.png'),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(8.0),
                          child: RoundButton(
                            color: Color(0xFFEFC27E),
                            child: Padding(
                              padding: EdgeInsets.all(8),
                              child: Image.asset('assets/map.png'),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(8.0),
                          child: RoundButton(
                            color: Color(0xFFB4A4DA),
                            child: Padding(
                              padding: EdgeInsets.all(8),
                              child: Image.asset('assets/user.png'),
                            ),
                            onTap: () async {
                              await update();
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                  Flexible(
                    child: Column(
                      children: [
                        Container(
                          height: 100.0,
                          width: double.infinity,
                          color: Color(0xFF0C4C75),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Text(
                                      singleton.myCurrentLocation.speed != null
                                          ? singleton.myCurrentLocation.speed!
                                              .toInt()
                                              .toString()
                                          : '0',
                                      style: TextStyle(
                                          fontFamily: 'FjallaOne',
                                          color: Colors.white,
                                          fontSize: 64.0,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(bottom: 8),
                                      child: Text(
                                        'km/h',
                                        style: TextStyle(
                                            fontFamily: 'FjallaOne',
                                            color: Colors.white,
                                            fontSize: 32.0,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.all(0.0),
                                      child: Icon(
                                        Icons.room_outlined,
                                        color: Colors.white,
                                        size: 64.0,
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.all(0.0),
                                      child: Icon(
                                        Icons.signal_cellular_0_bar_outlined,
                                        color: Colors.white,
                                        size: 64.0,
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.all(0.0),
                                      child: Transform.rotate(
                                        angle: 90 * math.pi / 180,
                                        child: Icon(
                                          Icons.battery_full_outlined,
                                          color: Colors.white,
                                          size: 64.0,
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.all(0.0),
                                      child: Icon(
                                        Icons.mail_outline,
                                        color: Colors.white,
                                        size: 64.0,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Text(
                                _timeString,
                                style: TextStyle(
                                    fontFamily: 'FjallaOne',
                                    color: Colors.white,
                                    fontSize: 64.0,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          height: MediaQuery.of(context).size.height - 100,
                          width: double.infinity,
                          child: MapScreen(),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          loading
              ? Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  color: Color(0x46000000),
                )
              : Text(''),
        ],
      ),
    );
  }
}
