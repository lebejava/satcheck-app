import 'dart:async';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:usb_serial/transaction.dart';
import 'package:usb_serial/usb_serial.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  UsbPort? _port;
  String _status = "Idle";
  List<Widget> _ports = [];
  List<Widget> _serialData = [];

  StreamSubscription<String>? _subscription;
  Transaction<String>? _transaction;
  UsbDevice? _device;

  TextEditingController _textController = TextEditingController();

  Timer? timer;
  bool isConnecting = false;
  int countConnecting = 0;

  bool isReadyToSend = false;

  List<String> commands = [
    'ati',
    'AT+CPIN?',
    'AT+CREG?',
    'AT+CSQ',
    'AT+NETOPEN',
    'AT+CIPOPEN=1,"UDP",,,6031',
    'AT+CIPSEND=1,5,"controlporsatelite.com",32000',
    '12345'
    'AT+CRESET',
  ];

  int commandIndex = -1;

  Future<bool> _connectTo(device) async {
    _serialData.clear();

    if (_subscription != null) {
      _subscription!.cancel();
      _subscription = null;
    }

    if (_transaction != null) {
      _transaction!.dispose();
      _transaction = null;
    }

    if (_port != null) {
      _port!.close();
      _port = null;
    }

    if (device == null) {
      _device = null;
      setState(() {
        _status = "Disconnected";
      });
      return true;
    }

    _port = await device.create("", 3);
    if (await (_port!.open()) != true) {
      setState(() {
        _status = "Failed to open port";
      });
      return false;
    }
    _device = device;

    await _port!.setDTR(false);
    await _port!.setRTS(false);
    await _port!.setPortParameters(
        115200, UsbPort.DATABITS_8, UsbPort.STOPBITS_1, UsbPort.PARITY_NONE);

    _transaction = Transaction.stringTerminated(
        _port!.inputStream as Stream<Uint8List>, Uint8List.fromList([13, 10]));

    _subscription = _transaction!.stream.listen((String line) {
      switch (commandIndex) {
        case 0:
          print('caso 0');
          if(line.indexOf('OK') != -1) {
            print('AT OK');
          }
          break;
        case 1:
          print('caso 1');
          if(line.indexOf('+CPIN: READY') != -1) {
            print('READY');
          }
          break;
        case 2:
          print('caso 2');
          if(line.indexOf('+CREG: 0,1') != -1 || line.indexOf('+CREG: 0,5') != -1) {
            print('REGISTER');
            setState(() {
              isReadyToSend = true;
            });
          }
          break;
        case 3:
          print('caso 3');
          break;
        case 4:
          print('caso 4');
          break;
      }

      // Display info
      setState(() {
        _serialData.add(Text(line));
        if (_serialData.length > 40) {
          _serialData.removeAt(0);
          print('REMOVE_AT');
        }
      });
    });

    setState(() {
      _status = "Connected";
    });
    return true;
  }

  void _getPorts() async {
    _ports = [];
    List<UsbDevice> devices = await UsbSerial.listDevices();
    if (!devices.contains(_device)) {
      _connectTo(null);
    }
    print(devices);

    devices.forEach(
      (device) {
        if (device.productName == 'A76XX Series LTE Module' && !isConnecting) {
          _connectTo(device);
        }
        _ports.add(
          ListTile(
            leading: Icon(Icons.usb),
            title: Text(device.productName!),
            subtitle: Text(device.manufacturerName!),
            trailing: ElevatedButton(
              child: Text(_device == device ? "Disconnect" : "Connect"),
              onPressed: () {
                _connectTo(_device == device ? null : device).then((res) {
                  _getPorts();
                });
              },
            ),
          ),
        );
      },
    );

    setState(
      () {
        print(_ports);
      },
    );
  }

  @override
  void initState() {
    super.initState();

    UsbSerial.usbEventStream!.listen((UsbEvent event) {
      _getPorts();
    });

    _getPorts();

    timer = Timer.periodic(Duration(seconds: 2), (Timer t) async {
      if (isConnecting) {
        if (countConnecting == 10) {
          print('Reconnecting');
          setState(() {
            isConnecting = false;
            countConnecting = 0;
            _getPorts();
          });
        } else {
          print('Count $countConnecting');
          setState(() {
            countConnecting++;
          });
        }
      }else{
        if(isReadyToSend) {
          String data = "${commands[5]}\r\n";
          await _port!.write(Uint8List.fromList(data.codeUnits));

          data = "${commands[6]}\r\n";
          await _port!.write(Uint8List.fromList(data.codeUnits));

          setState(() {
            isReadyToSend = false;
          });
        }else{
          rutine();
        }
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    _connectTo(null);
  }

  void rutine() async {
    if (_port == null) {
      print('No USB Port');
      return;
    }

    int commandIndexTemp = 0;
    if (commandIndex != commands.length - 1) {
      commandIndexTemp = commandIndex + 1;
    }

    String data = "${commands[commandIndexTemp]}\r\n";
    await _port!.write(Uint8List.fromList(data.codeUnits));

    setState(() {
      commandIndex = commandIndexTemp;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('USB Serial Plugin example app'),
        ),
        body: SingleChildScrollView(
          child: Center(
            child: Column(
              children: <Widget>[
                Text(
                    _ports.length > 0
                        ? "Available Serial Ports"
                        : "No serial devices available",
                    style: Theme.of(context).textTheme.headline6),
                ..._ports,
                Text('Status: $_status\n'),
                Text('info: ${_port.toString()}\n'),
                ListTile(
                  title: TextField(
                    controller: _textController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Text To Send',
                    ),
                  ),
                  trailing: ElevatedButton(
                    child: Text("Send"),
                    onPressed: _port == null
                        ? null
                        : () async {
                            if (_port == null) {
                              return;
                            }
                            String data = _textController.text + "\r\n";
                            await _port!
                                .write(Uint8List.fromList(data.codeUnits));
                            _textController.text = "";
                          },
                  ),
                ),
                Text("Result Data",
                    style: Theme.of(context).textTheme.headline6),
                ..._serialData,
              ],
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: rutine,
          child: Icon(Icons.star),
        ),
      ),
    );
  }
}
