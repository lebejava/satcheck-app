import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';
import 'dart:ui';

import 'package:background_location/background_location.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:satcheck/singleton.dart';
import 'package:http/http.dart' as http;

class MapScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MapScreen();
  }
}

class _MapScreen extends State<MapScreen> with SingleTickerProviderStateMixin {
  Singleton singleton = new Singleton();
  bool loading = false;

  late final MapController mapController;
  late final AnimationController _animationController;
  // late Timer globalRefresh;
  late Timer _requestCurrentLocationTimer;
  bool isEnable = false;

  var polygons = [
    Polygon(
        points: [
          LatLng(-16.404652, -71.533012),
          LatLng(-16.405445, -71.532234),
          LatLng(-16.405069826770188, -71.53185159335315),
          LatLng(-16.404276826770186, -71.53262959335315),
        ],
        color: Colors.transparent,
        borderColor: Colors.red,
        borderStrokeWidth: 2),
  ];

  List<Polyline> polyLines = [
    /*Polyline(
      points: [
        LatLng(-16.4070000, -71.5500000),
        LatLng(-16.4020000, -71.5500000),
        LatLng(-16.4030000, -71.5450000),
        LatLng(-16.4020000, -71.5350000),
        LatLng(-16.3988900, -71.5350000),
        LatLng(-16.3788900, -71.5300000),
      ],
      strokeWidth: 8.0,
      gradientColors: [
        const Color(0xFF8E2DE2),
        const Color(0xff4A00E0),
      ],
    ),*/
  ];

  final markers = [
    Marker(
        width: 80.0,
        height: 80.0,
        point: LatLng(-16.4020000, -71.5350000),
        builder: (ctx) => _Marker(Color(0xFFF92323))),
    Marker(
        width: 80.0,
        height: 80.0,
        point: LatLng(-16.4020000, -71.5500000),
        builder: (ctx) => _Marker(Color(0xFFF92323))),
  ];

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return mapUI();
  }

  @override
  void initState() {
    super.initState();
    _animationController =
        AnimationController(vsync: this, duration: const Duration(seconds: 3));
    _animationController.repeat();
    mapController = MapController();

    /*globalRefresh = Timer.periodic(Duration(seconds: 1), (timer) async {
      setState(() {});
    });*/

    _determinePosition();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  Future<void> _determinePosition() async {
    if (await Permission.location.serviceStatus.isEnabled) {
      var status = await Permission.location.status;
      if (status.isGranted) {
        _requestCurrentLocation();
      } else if (status.isDenied) {
        try {
          Map<Permission, PermissionStatus> request = await [
            Permission.location,
          ].request();
          status = await Permission.location.status;
          if (status.isGranted) {
            _requestCurrentLocation();
          } else {
            ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
              content: Text(
                  'Necesitamos acceder a tu ubicación. Por favor, habilita el permiso desde la configuración.'),
            ));
          }
        } catch (e) {
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
            content: Text(
                'Necesitamos acceder a tu ubicación. Por favor, habilita el permiso desde la configuración.'),
          ));
        }
      } else {
        openAppSettings();
      }
    } else {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text(
            'Necesitamos acceder a tu ubicación. Por favor, habilita el permiso desde la configuración.'),
      ));
    }
  }

  _requestCurrentLocation() async {
    setState(() {
      loading = true;
    });

    await BackgroundLocation.setAndroidNotification(
      title: 'Compartiendo ubicación en tiempo real',
      message: 'Servicio activo en segundo plano',
      icon: '@mipmap/ic_launcher',
    );
    // await BackgroundLocation.setAndroidConfiguration(1000);
    await BackgroundLocation.startLocationService(distanceFilter: 0);

    if (!isEnable) {
      _sendLocation();
      setState(() {
        isEnable = true;
        loading = false;
      });
    }

    _requestCurrentLocationTimer =
        Timer.periodic(Duration(seconds: 2), (timer) async {
      _sendLocation();
    });
  }

  _sendLocation() async {
    Location position = await await BackgroundLocation().getCurrentLocation();
    if (position != null && position.latitude != null) {
      // print('${position.latitude!}, ${position.longitude!}');
      setState(() {
        singleton.myCurrentLocation = position;
      });

      /*http.Response apiResponse = await ApiService.post(
          '/location',
          {
            "latitude": position.latitude!,
            "longitude": position.longitude!,
            "accuracy": position.accuracy!,
            "altitude": position.altitude!,
            "bearing": position.bearing!,
            "speed": position.speed!,
            "grpDateTime": position.time!.toInt(),
          },
          secure: true);*/

      // mapController.move(LatLng(position.latitude!, position.longitude!), 12.0);

    }
  }

  Widget mapUI() {
    return Stack(
      children: [
        FlutterMap(
          mapController: mapController,
          options: MapOptions(
              center: LatLng(-16.3988900, -71.5350000),
              zoom: 12.0,
              minZoom: 12.0,
              maxZoom: 14.0),
          layers: [
            TileLayerOptions(
              tileProvider: const FileTileProvider(),
              maxZoom: 14.0,
              urlTemplate: '${singleton.localDir}/map/{z}/{x}/{y}.png',
            ),
            /*TileLayerOptions(
                urlTemplate:
                    "https://api.mapbox.com/styles/v1/ftamayom/cl2cj616e003b14mygtl3fu75/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoiZnRhbWF5b20iLCJhIjoiY2twcmQzZHE5MGMyZDJwbzBlZXZocDN1diJ9.I47XS_CBlqs9pkkOYWANfw",
                additionalOptions: {
                  'accessToken':
                      'pk.eyJ1IjoiZnRhbWF5b20iLCJhIjoiY2twcmQzZHE5MGMyZDJwbzBlZXZocDN1diJ9.I47XS_CBlqs9pkkOYWANfw',
                  'id': 'mapbox.mapbox-streets-v8'
                },
                /*attributionBuilder: (_) {
              return Text("© OpenStreetMap contributors");
            },*/
              ),*/
            PolygonLayerOptions(polygons: polygons),
            PolylineLayerOptions(
              polylines: polyLines,
            ),
            MarkerLayerOptions(
              markers: singleton.myCurrentLocation.latitude != null
                  ? [
                      Marker(
                          width: 80.0,
                          height: 80.0,
                          point: LatLng(singleton.myCurrentLocation.latitude!,
                              singleton.myCurrentLocation.longitude!),
                          builder: (ctx) =>
                              _MyLocationMarker(_animationController)),
                      ...markers
                    ]
                  : markers,
            ),
          ],
        ),
      ],
    );
  }
}

class _MyLocationMarker extends AnimatedWidget {
  const _MyLocationMarker(Animation<double> animation, {Key? key})
      : super(key: key, listenable: animation);

  @override
  Widget build(BuildContext context) {
    final value = (listenable as Animation<double>).value;
    final newValue = lerpDouble(0.5, 1.0, value)!;
    final firstSize = 40.0;
    final secondSize = 60.0;
    return Stack(
      children: [
        Center(
          child: Container(
            height: firstSize * newValue,
            width: firstSize * newValue,
            decoration: BoxDecoration(
                color: Color(0xFF006EB9).withOpacity(0.5),
                shape: BoxShape.circle),
          ),
        ),
        Center(
          child: Container(
            height: secondSize * value,
            width: secondSize * value,
            decoration: BoxDecoration(
                color: Color(0xFF006EB9).withOpacity(0.2),
                shape: BoxShape.circle),
          ),
        ),
        Center(
          child: Container(
            height: 20.0,
            width: 20.0,
            decoration: BoxDecoration(
              color: Color(0xFF006EB9),
              shape: BoxShape.circle,
              boxShadow: [
                BoxShadow(color: Color(0xFF006EB9), blurRadius: 16.0),
              ],
              border: Border.all(
                width: 0.5,
                color: Colors.black,
                style: BorderStyle.solid,
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class _Marker extends StatelessWidget {
  _Marker(this.color);

  final Color color;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        height: 20.0,
        width: 20.0,
        decoration: BoxDecoration(
          color: color,
          shape: BoxShape.circle,
          boxShadow: [
            BoxShadow(color: color, blurRadius: 16.0),
          ],
          border: Border.all(
            width: 0.5,
            color: Colors.black,
            style: BorderStyle.solid,
          ),
        ),
      ),
    );
  }
}
